#include <stdio.h>
#include <stdlib.h>

FILE* open_input(int argc, char* argv[]) {
    if (argc != 2) {
        printf("expected exactly one argument\n");
        exit(1);
    }
    FILE* input = fopen(argv[1], "r");
    if (input == NULL) {
        printf("failed to open file\n");
        exit(1);
    }
    return input;
}
