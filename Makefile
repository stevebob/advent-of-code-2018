CC=gcc
CFLAGS=-Wall -Wextra -Werror -O2 -g
TARGETS=avl_tree_test day1-1 day1-2 day2-1 day2-2 day3-1 day3-2

all: $(TARGETS)

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

avl_tree_test: avl_tree_test.o int_set.o
	$(CC) -o $@ $^ $(CFLAGS)

day1-1: day1-1.o input.o
	$(CC) -o $@ $^ $(CFLAGS)

day1-2: day1-2.o input.o vector.o int_set.o
	$(CC) -o $@ $^ $(CFLAGS)

day2-1: day2-1.o input.o
	$(CC) -o $@ $^ $(CFLAGS)

day2-2: day2-2.o input.o vector.o str_set.o
	$(CC) -o $@ $^ $(CFLAGS)

day3-1: day3-1.o input.o
	$(CC) -o $@ $^ $(CFLAGS)

day3-2: day3-2.o input.o
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean watch
clean:
	rm -f *.o $(TARGETS)

watch:
	while true; do \
		make $(WATCHMAKE); \
		inotifywait -qre close_write .; \
	done
