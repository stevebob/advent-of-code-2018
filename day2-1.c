#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "input.h"

#define NUM_LETTERS 26
#define BUFSIZE 64

struct counts {
    bool _2;
    bool _3;
};

struct counts count_repeated(char* s) {
    uint8_t counts[NUM_LETTERS] = {0};
    struct counts ret = {false, false};
    for (char* c = s; *c != '\0'; c++) {
        if (islower(*c)) {
            counts[*c - 'a']++;
        }
    }
    for (int i = 0; i < NUM_LETTERS; i++) {
        if (counts[i] == 2) {
            ret._2 = true;
        }
        if (counts[i] == 3) {
            ret._3 = true;
        }
    }
    return ret;
}

int main(int argc, char* argv[]) {
    FILE* input = open_input(argc, argv);
    char buf[BUFSIZE];
    int _2 = 0;
    int _3 = 0;
    while (fgets(buf, BUFSIZE, input)) {
        struct counts counts = count_repeated(buf);
        _2 += counts._2;
        _3 += counts._3;
    }
    printf("%d\n", _2 * _3);
    return 0;
}
