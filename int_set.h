#include <stdbool.h>

typedef struct int_avl_tree_set int_set_t;

int_set_t* int_set_make();
void int_set_free(int_set_t* t);
bool int_set_contains(int_set_t* t, int key);
bool int_set_insert(int_set_t* t, int key);
bool int_set_remove(int_set_t* t, int key);
void int_set_check_invariants(int_set_t* t);
