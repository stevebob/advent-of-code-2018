#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

typedef struct vector {
    void* data;
    size_t capacity_elements;
    size_t len_elements;
    size_t element_size;
    bool data_are_owned_pointers;
} vector_t;

#define DEFAULT_CAPACITY_ELEMENTS 32

vector_t* vector_make_empty(size_t element_size, bool data_are_owned_pointers) {
    vector_t* vector = malloc(sizeof(vector_t));
    vector->data = malloc(element_size * DEFAULT_CAPACITY_ELEMENTS);
    vector->element_size = element_size;
    vector->capacity_elements = DEFAULT_CAPACITY_ELEMENTS;
    vector->len_elements = 0;
    vector->data_are_owned_pointers = data_are_owned_pointers;
    return vector;
}

void vector_free(vector_t* t) {
    if (t->data_are_owned_pointers) {
        void** data = (void**)t->data;
        for (size_t i = 0; i < t->len_elements; i++) {
            free(data[i]);
        }
    }
    free(t->data);
    free(t);
}

void vector_push(vector_t* t, void* element_ptr) {
    assert(t->len_elements <= t->capacity_elements);
    if (t->len_elements == t->capacity_elements) {
        t->capacity_elements *= 2;
        t->data = realloc(t->data, t->capacity_elements * t->element_size);
        assert(t->data != NULL);
    }
    char* next_element_start =
        ((char*)t->data) + (t->element_size * t->len_elements);
    memmove(next_element_start, element_ptr, t->element_size);
    t->len_elements += 1;
}

#define MAX_STRING 1024

void vector_push_str(vector_t* t, char* s) {
    char* s_copy = strndup(s, MAX_STRING);
    vector_push(t, &s_copy);
}

size_t vector_len(vector_t* t) { return t->len_elements; }

void* vector_data(vector_t* t) { return t->data; }
