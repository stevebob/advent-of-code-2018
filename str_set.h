#include <stdbool.h>

typedef struct str_avl_tree_set str_set_t;

str_set_t* str_set_make();
void str_set_free(str_set_t* t);
bool str_set_contains(str_set_t* t, char* key);
bool str_set_insert(str_set_t* t, char* key);
bool str_set_remove(str_set_t* t, char* key);
void str_set_check_invariants(str_set_t* t);
